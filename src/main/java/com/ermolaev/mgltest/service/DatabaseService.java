package com.ermolaev.mgltest.service;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Created by ermolaev on 11/23/16.
 */
@Service
public class DatabaseService {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    private HikariDataSource dataSource;

    public DatabaseService() {
        ClassLoader classLoader = getClass().getClassLoader();
        Properties properties = new Properties();

        try(InputStream inputStream = classLoader.getResource("db.properties").openStream()) {
            properties.load(inputStream);
            HikariConfig config = new HikariConfig(properties);
            dataSource = new HikariDataSource(config);
        } catch (IOException e) {
            log.error("Cannot connect to database : ", e);
        }

    }

    public Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }
}
