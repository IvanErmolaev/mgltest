package com.ermolaev.mgltest.controller;

import com.ermolaev.mgltest.domain.Constants;
import com.ermolaev.mgltest.domain.User;
import com.ermolaev.mgltest.exception.UserNotFoundException;
import com.ermolaev.mgltest.service.DatabaseService;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ermolaev on 11/22/16.
 */
@RestController
@RequestMapping("/permanent")
public class PermanentUserDataController {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private DatabaseService databaseService;

    @RequestMapping(value = "/add-user", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public void addUser(HttpServletRequest request) throws SQLException {
        User user = new User();

        String ageString = request.getParameter("age");

        user.setName(request.getParameter("name"));
        user.setSurname(request.getParameter("surname"));
        user.setAge(ageString != null ? Integer.parseInt(ageString) : 0);

        Connection connection = databaseService.getConnection();
        try {
            connection.setAutoCommit(false);
            PreparedStatement statement = connection.prepareStatement(Constants.INSERT_USER);
            statement.setString(1, user.getName());
            statement.setString(2, user.getSurname());
            statement.setInt(3, user.getAge());
            statement.execute();
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
            log.error("Cannot insert user : ", e);
            throw e;
        }
        finally {
            connection.close();
        }
    }

    @RequestMapping("/get-users")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody Map<Integer, User> getUsers() throws SQLException {
        Map<Integer, User> resultMap = new HashMap<>();

        Connection connection = databaseService.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement(Constants.SELECT_USERS);

            ResultSet resultSet = statement.executeQuery();
            if(resultSet != null) {
                while(resultSet.next()) {
                    User user = new User();
                    user.setId(resultSet.getInt(1));
                    user.setName(resultSet.getString(2));
                    user.setSurname(resultSet.getString(3));
                    user.setAge(resultSet.getInt(4));

                    resultMap.put(user.getId(), user);
                }
            }
        }
        catch (SQLException e) {
            log.error("Cannot select users : ", e);
            throw e;
        }
        finally {
            connection.close();
        }
        return resultMap;
    }

    @RequestMapping("/get-user-by-id")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody User getUserById(
            @RequestParam("id") Integer id
    ) throws SQLException {

        Connection connection = databaseService.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement(Constants.SELECT_USER_BY_ID);
            statement.setInt(1, id);

            ResultSet resultSet = statement.executeQuery();
            if(resultSet != null && resultSet.next()) {
                User user = new User();
                user.setId(resultSet.getInt(1));
                user.setName(resultSet.getString(2));
                user.setSurname(resultSet.getString(3));
                user.setAge(resultSet.getInt(4));

                return user;
            }
        }
        catch (SQLException e) {
            log.error("Cannot select user by id : ", e);
            throw e;
        }
        finally {
            connection.close();
        }

        throw new UserNotFoundException();
    }


    @ExceptionHandler(NumberFormatException.class)
    @ResponseStatus(value = HttpStatus.NOT_ACCEPTABLE, reason = "user_id or/and age are invalid")
    public void handleNumberFormatException() {}

    @ExceptionHandler(UserNotFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "user not found by id")
    public void handleUserNotFoundException() {}

    @ExceptionHandler(SQLException.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public void handleSqlException() {}
}
