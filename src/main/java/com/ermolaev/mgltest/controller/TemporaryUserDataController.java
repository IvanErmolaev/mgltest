package com.ermolaev.mgltest.controller;

import com.ermolaev.mgltest.domain.User;
import com.ermolaev.mgltest.exception.UserNotFoundException;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by ermolaev on 11/22/16.
 */
@RestController
@RequestMapping("/temporary")
public class TemporaryUserDataController {

    private static Set<User> users = Collections.synchronizedSet(new HashSet<>());

    @RequestMapping(value = "/add-user", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public void addUser(HttpServletRequest request) {
        User user = new User();

        String userIdString = request.getParameter("user_id");
        String ageString = request.getParameter("age");

        user.setId(userIdString != null ? Integer.parseInt(userIdString) : 0);
        user.setName(request.getParameter("name"));
        user.setSurname(request.getParameter("surname"));
        user.setAge(ageString != null ? Integer.parseInt(ageString) : 0);

        users.add(user);
    }

    @RequestMapping("/get-users")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody Map<Integer, User> getUsers() {
        return users.stream().collect(Collectors.toMap(User::getId, Function.identity()));
    }

    @RequestMapping("/get-user-by-id")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody User getUserById(
            @RequestParam("id") Integer id
    ) {
        Optional<User> foundUser = users.stream().filter(user -> id.equals(user.getId())).findFirst();
        if(!foundUser.isPresent()) {
           throw new UserNotFoundException();
        }
        return foundUser.get();
    }


    @ExceptionHandler(NumberFormatException.class)
    @ResponseStatus(value = HttpStatus.NOT_ACCEPTABLE, reason = "user_id or/and age are invalid")
    public void handleNumberFormatException() {}

    @ExceptionHandler(UserNotFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "user not found by id")
    public void handleUserNotFoundException() {}
}
