package com.ermolaev.mgltest.domain;

/**
 * Created by ermolaev on 11/23/16.
 */
public class Constants {

    public static final String INSERT_USER = "INSERT INTO mgltest.users (name, surname, age) VALUES (?, ?, ?)";
    public static final String SELECT_USERS = "SELECT id, name, surname, age FROM mgltest.users";
    public static final String SELECT_USER_BY_ID = "SELECT id, name, surname, age FROM mgltest.users WHERE id = ?";
}
